export type Point = { x: number, y: number };
export type ItemPoint<TItem> = Point & { value: TItem };

export class Matrix<TItem> {
    protected matrix: TItem[][]
    protected width: number;
    protected height: number;
    protected defaultValue: TItem;

    constructor(width: number, height: number, defaultValue?: TItem) {
        this.width = width;
        this.height = height;
        this.matrix = [];
        for (let y = 0; y < height; y++) {
            const row = [];
            for (let x = 0; x < width; x++) {
                if (typeof defaultValue === 'object') {
                    row.push(Object.assign({}, defaultValue));
                } else {
                    row.push(defaultValue);
                }
            }
            this.matrix.push(row);
        }
    }

    public set(x: number, y: number, value: TItem) {
        this.matrix[x][y] = value;
    }

    public get(x: number, y: number) {
        return this.matrix[x][y];
    }

    public remove(x: number, y: number) {
        this.matrix[x][y] = this.defaultValue;
    }

    public copy() {
        const copy = new Matrix<TItem>(this.width, this.height);

        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                copy.set(x, y, this.get(x, y));
            }
        }

        return copy;
    }

    public neighbors(x: number, y: number): ItemPoint<TItem>[] {
        const result = [];
        for (let row = y - 1; row <= y + 1; row++) {
            for (let column = x - 1; column <= x + 1; column++) {
                if ((column === x && row === y)
                    || column < 0 || row < 0 || column >= this.width || row >= this.height) {
                    continue;
                }

                result.push({ x: column, y: row, value: this.get(column, row) });
            }
        }

        return result;
    }

    public toString(separator: string = ' ', key?: string) {
        let result = '';

        for (let y = 0; y < this.height; y++) {
            let row = '';
            for (let x = 0; x < this.width; x++) {
                const value = this.get(x, y) && key ? this.get(x, y)[key] : this.get(x, y);
                const stringValue = typeof value ? JSON.stringify(value) : value;
                row += (stringValue || '     ') + separator;
            }
            result += row + '\n';
        }

        return result;
    }
}