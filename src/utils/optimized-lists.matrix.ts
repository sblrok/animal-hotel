import { Matrix, Point } from './matrix';
import { DoublyLinkedList, Node } from './doubly-linked-list';

/**
 * Optimized Lists Matrix
 * Матрица со списками заполненных и пустых ячеек
 */
export class OLMatrix<TItem> extends Matrix<TItem> {
    private filledList: DoublyLinkedList<Point>
    private filledMatrix: Matrix<Node<Point>>;
    private emptyList: DoublyLinkedList<Point>;
    private emptyMatrix: Matrix<Node<Point>>;

    public get emptyCount() {
        return this.emptyList.count;
    }

    public get empty() {
        return this.emptyList.iterate();
    }

    public get filled() {
        return this.filledList.iterate();
    }

    public get filledCount() {
        return this.filledList.count
    }

    constructor(width: number, height: number) {
        super(width, height);

        this.filledList = new DoublyLinkedList();
        this.filledMatrix = new Matrix(width, height);
        this.emptyList = new DoublyLinkedList();
        this.emptyMatrix = new Matrix(width, height);

        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                const node = this.emptyList.append({ x, y });
                this.emptyMatrix.set(x, y, node);
            }
        }
    }

    public remove(x: number, y: number) {
        super.remove(x, y);

        const filledNode = this.filledMatrix.get(x, y);
        if (!filledNode) {
            throw new Error('Trying to clear empty cell');
        }

        this.filledList.remove(filledNode);
        this.filledMatrix.remove(x, y);

        const emptyNode = this.emptyList.prepend({x, y});
        this.emptyMatrix.set(x, y, emptyNode);
    }

    public set(x: number, y: number, value: TItem) {
        super.set(x, y, value);
        const emptyNode = this.emptyMatrix.get(x, y);
        if (!emptyNode) {
            throw new Error('Trying to fill not empty cell');
        }

        this.emptyList.remove(emptyNode);
        this.emptyMatrix.remove(x, y);

        const filledNode = this.filledList.prepend({x, y});
        this.filledMatrix.set(x, y, filledNode);
    }
}