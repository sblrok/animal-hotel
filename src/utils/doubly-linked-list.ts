export class Node<T> {
    constructor(
        public value: T,
        public next?: Node<T>,
        public previous?: Node<T>,
    ) {}
}

export class DoublyLinkedList<TItem> {
    protected _count = 0;
    protected _head: Node<TItem>;
    protected _tail: Node<TItem>;

    public get count() {
        return this._count;
    }

    public get head() {
        return this._head;
    }

    public get tail() {
        return this._tail;
    }

    public append(value: TItem) {
        const node = new Node(value);

        if (!this._head) {
            this._head = node;
            this._tail = node;
        } else {
            this._tail.next = node;
            node.previous = this._tail;
            this._tail = node;
        }

        this._count++;

        return node;
    }

    public prepend(value: TItem) {
        const node = new Node(value);

        if (!this._head) {
            this._head = node;
            this._tail = node;
        } else {
            node.next = this._head;
            this._head.previous = node;
            this._head = node;
        }

        this._count++;

        return node;
    }

    public remove(node: Node<TItem>) {
        if (this._count === 1) {
            delete this._head;
            delete this._tail;
        }
        if (node.next) {
            node.next.previous = node.previous;
        } else {
            this._tail = node.previous;
        }
        if (node.previous) {
            node.previous.next = node.next;
        } else {
            this._head = node.next;
        }

        this._count--;
    }

    // Можно реализовать через массивы, что будет чище, но так быстрее
    public iterate() {
        const iterator = {
            *[Symbol.iterator](): IterableIterator<{ x: number, y: number }> {
                let node = this.head;
                while (node) {
                    yield node.value;
                    node = node.next;
                }
            }
        };
        iterator[Symbol.iterator] = iterator[Symbol.iterator].bind(this);

        return iterator;
    }

    public toArray() {
        const result = [];

        let node = this._head;
        while (node) {
            result.push(node.value);
            node = node.next;
        }

        return result;
    }
}
