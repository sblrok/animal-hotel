/**
 * Возвращает объект с ключами types и значением defaultValue или 0
 * Например: { 'A': 0, 'B': 0 }
 * @param types
 */
export function getCounter<TType extends string>(types: readonly TType[], defaultValue = 0) {
    return types.reduce((result, type) => {
        result[type] = defaultValue;
        return result;
    }, {} as { [key in TType]: number});
}

/**
 * Возвращает координаты следующей ячейки матрицы
 * @param currentX Текущее значение X
 * @param currentY Текущее значение Y
 * @param size Ширина и высота матрицы
 */
export function getNextPoint(currentX: number, currentY: number, size: number);
/**
 * Возвращает координаты следующей ячейки матрицы
 * @param currentX Текущее значение X
 * @param currentY Текущее значение Y
 * @param width Ширина матрицы
 * @param height Высота матрицы
 */
export function getNextPoint(currentX: number, currentY: number, width: number, height: number);
export function getNextPoint(currentX: number, currentY: number, width: number, height?: number) {
    if (isNaN(height)) {
        height = width;
    }

    let nextX = currentX + 1;
    let nextY = currentY;
    if (nextX === width) {
        nextX = 0;
        nextY++;
    }
    if (nextY !== height) {
        return { x: nextX, y: nextY };
    }
}

export function arrayFromArrayObject(obj: { [key: string]: number }) {
    return Object.keys(obj).map((key) => obj[key]);
}