import { Guest, GuestService, GuestTypes } from './guest.service';
import { arrayFromArrayObject, getCounter } from '../utils/common';

/**
 * Сервис бронирования с использованием эвристического алгоритма для нахождения оптимальной ячейки (комнаты).
 * Может давать не самый оптимальный результат
 */
export class HeuristicGuestService extends GuestService {
    // Сумма соседей для каждой свободной клетки
    private neighborsCounter = getCounter(GuestTypes);

    public tryCheckIn(type: Guest, to: number) {
        const result = super.tryCheckIn(type, to);

        if (result) {
            const count = this.matrix.neighbors(result.x, result.y)
                .filter((item) => !item.value).length;
            this.neighborsCounter[type] += count;
        }

        return result;
    }

    protected findRoom(type: Guest) {
        let min = Number.MAX_VALUE;
        let minX = -1;
        let minY = -1;
        for (let y = 0; y < this.size; y++) {
            for (let x = 0; x < this.size; x++) {

                if (!this.isRoomAvailable(x, y, type)) {
                    continue;
                }

                const counter = Object.assign({}, this.neighborsCounter);

                for (const neighbor of this.matrix.neighbors(x, y)) {
                    if (neighbor.value) {
                        continue;
                    }

                    counter[type]++;
                }

                const countArray = arrayFromArrayObject(counter);

                const max = Math.max(...countArray);
                if (max < min) {
                    min = max;
                    minX = x;
                    minY = y;
                }
            }
        }

        if (min < Number.MAX_VALUE) {
            return { x: minX, y: minY };
        }
    }
}