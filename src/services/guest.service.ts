import { Matrix, Point } from '../utils/matrix';
import { getCounter } from '../utils/common';
import { OLMatrix } from '../utils/optimized-lists.matrix';

export const GuestTypes = ['cat', 'dog'] as const;
export type Guest = (typeof GuestTypes)[number];
export type GuestCounter = { [key in Guest]: number };

/**
 * Серис бронирования
 */
export abstract class GuestService {
    /**
     * Количество разрешенных соседей того же типа
     */
    protected ALLOWED_SAME_TYPE = 4 as const;
    /**
     * Количество разрешенных соседей другого типа
     */
    protected ALLOWED_ANOTHER_TYPE = 3 as const;

    protected size: number;
    protected matrix: OLMatrix<{ type: Guest, to: number }>; // to: Date
    protected neighborsMatrix: Matrix<GuestCounter>;

    constructor(size: number) {
        this.size = size;
        this.matrix = new OLMatrix(size, size);
        this.neighborsMatrix = new Matrix(size, size, getCounter(GuestTypes));
    }

    /**
     * Подбирает оптимальную комнату для заселения.
     * Возвращает координаты, если найдена
     * @param type Тип гостя для заселения
     * @param to До какого дня заселить
     */
    public tryCheckIn(type: Guest, to: number): false | Point {
        const room = this.findRoom(type);

        if (!room) {
            return false;
        }

        // Заселяем гостя
        this.matrix.set(room.x, room.y, { type, to });
        // Обновляем соседей
        for (const item of this.neighborsMatrix.neighbors(room.x, room.y)) {
            item.value[type]++;
        }

        return room;
    }

    /**
     * Очистить ячейки от гостей у которых прошел срок проживания
     * @param currentDay Номер текущего дня
     */
    public checkOutOutdate(currentDay: number) {
        for (const { x, y } of this.matrix.filled) {
            const guest = this.matrix.get(x, y);
            if (guest?.to < currentDay) {
                this.matrix.remove(x, y);
                for (const item of this.neighborsMatrix.neighbors(x, y)) {
                    item.value[guest.type]--;
                }
            }
        }
    }

    /**
     * Проверяет, свободна ли комната
     */
    protected isRoomAvailable(x: number, y: number, type: Guest) {
        if (this.matrix.get(x, y)) {
            return false;
        }

        const counter = getCounter(GuestTypes);
        for (const item of this.matrix.neighbors(x, y)) {
            if (!item.value) {
                continue;
            }

            counter[item.value.type]++;

            const neighbors = this.neighborsMatrix.get(item.x, item.y);
            const virtualNeighbors = {
                ...neighbors,
                [type]: neighbors[type] + 1,
            };

            if (!this.validateCounter(virtualNeighbors, item.value.type)) {
                return false;
            }
        }


        return this.validateCounter(counter, type);
    }

    private validateCounter(counter: { [key in Guest]: number }, type: Guest) {
        for (const item of GuestTypes) {
            if (item === type) {
                if (counter[item] > this.ALLOWED_SAME_TYPE) {
                    return false;
                }
            } else {
                if (counter[item] > this.ALLOWED_ANOTHER_TYPE) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Вывести матрицу
     */
    public show() {
        console.log(this.matrix.toString(' ', 'type'));
        console.log('Свободных ячеек:', this.matrix.emptyCount);
        console.log('Занятых ячеек:', this.matrix.filledCount);
    }

    protected abstract findRoom(type: Guest): Point | undefined;
}