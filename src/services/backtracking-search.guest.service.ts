import { Guest, GuestService, GuestTypes } from './guest.service';
import { arrayFromArrayObject, getNextPoint } from '../utils/common';

/**
 * Сервис бронирования с использованием алгоритма обратного поиска для нахождения оптимальной ячейки (комнаты)
 */
export class BacktrackingSearchGuestService extends GuestService {
    private iterationsCount = 0;

    protected findRoom(type: Guest) {
        let maxAmount = 0;
        let foundX = 0;
        let foundY = 0;

        // Для каждой доступной ячейки считаем максимальное количество новых животных,
        // которых мы можем заселить после заселения текущего животного
        for (let y = 0; y < this.size; y++) {
            for (let x = 0; x < this.size; x++) {
                const available = this.isRoomAvailable(x, y, type);

                if (!available) {
                    continue;
                }

                const amount = this.recursiveAmountCheck(x, y, type);

                if (maxAmount < amount) {
                    maxAmount = amount;
                    foundX = x;
                    foundY = y;
                }
            }
        }


        if (maxAmount !== 0) {
            return { x: foundX, y: foundY };
        }
    }

    private recursiveAmountCheck(x: number, y: number, type: Guest, amount = 0) {
        // Количество итераций для статистики
        this.iterationsCount++;

        const isAvailable = this.isRoomAvailable(x, y, type);

        // Если ячейка не доступна для указанного типа животного
        if (!isAvailable) {
            return amount;
        }

        // Получаем значение следующей ячейки, которую нужно проверить
        let nextPoint = getNextPoint(x, y, this.size);

        // Если мы в конце матрицы
        if (!nextPoint) {
            return amount + 1;
        }

        // Здесь мы "заселяем" животное,
        // что бы метод isRoomAvailable работал корректно в дальнейших уровнях рекурсии
        this.matrix.set(x, y, { type, to: 0 });

        let maxAmount = 0;

        while (nextPoint) {
            // Считаем количество возможных животных для каждого типа
            const counter = {};
            for (const GuestType of GuestTypes) {
                counter[GuestType] = this.recursiveAmountCheck(
                    nextPoint.x,
                    nextPoint.y,
                    GuestType,
                    amount + 1
                );
            }

            const counterArray = arrayFromArrayObject(counter);
            const minOfTypes = Math.min(...counterArray);

            // Берем минимальное количество по типу за максимум на этом уровне
            if (maxAmount < minOfTypes) {
                maxAmount = minOfTypes;
            }

            nextPoint = getNextPoint(nextPoint.x, nextPoint.y, this.size);
        }

        // После каждого уровня рекурсии "выселяем" животное обратно
        this.matrix.remove(x, y);

        return maxAmount;
    }
}