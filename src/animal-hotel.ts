import { Guest, GuestService } from './services/guest.service';

export class AnimalHotel {
    private guestService: GuestService;
    private currentDay: number = 0;

    // Используем Dependency Injection на случай изменения обработчика
    constructor(guestService: GuestService) {
        this.guestService = guestService;
    }

    /**
     * Заселить
     */
    public checkIn(guest: Guest, days: number) {
        const result = this.guestService.tryCheckIn(guest, this.currentDay + days - 1);
        if (!result) {
            throw new Error('Извините, мы не можем вас заселить');
        }

        return result;
    }

    public nextDay() {
        // В реальном проекте можно использовать Date.now();
        this.currentDay++;
        this.guestService.checkOutOutdate(this.currentDay);
    }

    public show() {
        this.guestService.show();
    }
}