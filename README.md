# Задача

В гостинице для собак и кошек есть N этажей, на каждом этаже по N комнат в ряд.  
Соответственно, гостиница представима в виде двумерного массива N*N, и у каждой
комнаты есть максимум 8 соседних комнат (слева-сверху, сверху, справа-сверху,
слева, справа, слева-снизу, снизу, справа-снизу).

Каждый день в гостиницу пытается заселиться нескончаемый поток собак и кошек,
каждое животное пытается заселиться на случайный срок от 7 до 14 дней.  
Необходимо реализовать метод заселения checkIn(animal: 'cat' | 'dog', days: number),
пытающийся выделить подходящую комнату, с учетом следующего правила:  
у животного должно быть не более четырех себе подобных соседей и не более
трех себе не подобных соседей.  
Например, у кошки не должно быть более 4
соседей-кошек, а также не должно быть более 3 соседей-собак.

**Пожелания:**

* реализовать класс AnimalHotel, включающий метод checkIn,
  а также все необходимые структуры данных и другие требующиеся методы.
* постараться разработать алгоритм, позволяющий добиться максимальной
  средней заполненности гостиницы в долгосрочном периоде
* постараться добиться хорошей вычислительной эффективности работы алгоритмов
* написать код, моделирующий существование гостиницы (заселения, выселения) в течение T дней.

