import { AnimalHotel } from '../src/animal-hotel';
import { BacktrackingSearchGuestService } from '../src/services/backtracking-search.guest.service';
import { expect } from 'chai';

describe('Существование гостиницы по дням', () => {
    const size = 3;
    const backtrackingSearch = new BacktrackingSearchGuestService(size);
    const hotel = new AnimalHotel(backtrackingSearch);

    it('3 дня гостицы', () => {
        console.time('1) Время');

        // Заполняем полностью - 8 гостей
        hotel.checkIn('cat', 3);
        hotel.checkIn('cat', 3);
        hotel.checkIn('cat', 3);
        hotel.checkIn('cat', 3);
        hotel.checkIn('cat', 2);
        hotel.checkIn('cat', 2);
        hotel.checkIn('cat', 2);
        hotel.checkIn('cat', 1);
        // hotel.show();

        hotel.nextDay(); // После первого дня остается 7 гостей
        // hotel.show();

        hotel.checkIn('cat', 10);
        expect(() => {
            hotel.checkIn('cat', 10);
        }).to.throw(Error);

        hotel.nextDay(); // После второго дня остается 4 + 1
        // hotel.show();

        hotel.checkIn('cat', 10);
        hotel.checkIn('cat', 10);
        hotel.checkIn('cat', 10);
        expect(() => {
            hotel.checkIn('cat', 10);
        }).to.throw(Error);

        hotel.nextDay(); // После третьего дня остается 0 + 4
        // hotel.show();


        hotel.checkIn('cat', 10);
        hotel.checkIn('cat', 10);
        hotel.checkIn('cat', 10);
        hotel.checkIn('cat', 10);
        expect(() => {
            hotel.checkIn('cat', 10);
        }).to.throw(Error);

        // hotel.show();

        console.timeEnd('1) Время');
    });
});