import { AnimalHotel } from '../src/animal-hotel';
import { BacktrackingSearchGuestService } from '../src/services/backtracking-search.guest.service';
import { expect } from 'chai';

describe('BacktrackingSearch', () => {
    const size = 3;
    let hotel;
    beforeEach(() => {
        const backtrackingSearch = new BacktrackingSearchGuestService(size);
        hotel = new AnimalHotel(backtrackingSearch);
    })

    it('Заполнение отеля 3х3 двумя типами', () => {
        console.time('1) Время');

        hotel.checkIn('cat', 7);
        hotel.checkIn('dog', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('dog', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('dog', 7);
        hotel.checkIn('cat', 7);
        // hotel.show();

        console.timeEnd('1) Время');
    });

    it('Заполнение отеля 3х3 котами', () => {
        console.time('2) Время');

        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        // hotel.show();

        console.timeEnd('2) Время');
    });

    it('Заселение лишнего гостя', () => {
        console.time('3) Время');

        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);
        hotel.checkIn('cat', 7);

        expect(() => {
            hotel.checkIn('cat', 7);
        }).to.throw(Error);


        console.timeEnd('3) Время');
    });

});